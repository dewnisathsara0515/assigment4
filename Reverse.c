#include <stdio.h>
int main() {
    int number, c = 0, x;
    printf("Enter an integer: ");
    scanf("%d", &number);
    while (number != 0)
        {
        x = number % 10;
        c = c * 10 + x;
        number = number/10;
       }
    printf("Reversed number = %d", c);
    return 0;
}
