#include <stdio.h>
int main()
{
    int number,answer,c=1;
    printf("Enter Positive Integer = ");
    scanf("%d", &number);
    while (answer!= number*number)
    {

        printf("%d * %d = %d \n", number, c, number * c);
        answer=number*c;
        c=c+1;

    }

    return 0;
}

/*1 2 3 4 5
2 4 6 8 10
3 6 9 12 15
4
5*/
